// cruff, eromero4
console.log('entered main.js');

var submitButton = document.getElementById('send-button');
submitButton.onmouseup = getInfoFromForm;
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearAll;

function clearAll(){
  document.getElementById("response-label").innerHTML = "";
  document.getElementById("answer-label").innerHTML = "";
  //document.getElementById("input-port-number").value = "";
  document.getElementById("text-message-body").value = "";
  document.getElementById("input-key").value = "";
  document.getElementById("checkbox-use-key").checked = false;
  document.getElementById("checkbox-use-message").checked = false;
  document.getElementById("radio-get").checked = true;
  document.getElementById("select-server-address").value = "http://student04.cse.nd.edu";
}


function getInfoFromForm(){
    console.log('entered getInfoFromForm');
    var inputAddr = document.getElementById("select-server-address").value;
    console.log('Selected server: ' + inputAddr);
    var inputPort = document.getElementById("input-port-number").value;
    console.log('Selected port: ' + inputPort);

    //getting radio radio button
    var inputRequestType = document.getElementById("radio-get").value; // setting get as default
    if(document.getElementById("radio-put").checked){
      inputRequestType = document.getElementById("radio-put").value;
    }
    else if(document.getElementById("radio-post").checked){
      inputRequestType = document.getElementById("radio-post").value;
    }
    else if(document.getElementById("radio-delete").checked){
      inputRequestType = document.getElementById("radio-delete").value;
    }
    console.log('Selected request type: ' + inputRequestType);

    if(document.getElementById("checkbox-use-key").checked){
      var inputKey = document.getElementById("input-key").value;
      console.log('Key entered: ' + inputKey);
    }
    else{
      var inputKey = null;
      //console.log('lalalalalalalala');
    }
    if(document.getElementById("checkbox-use-message").checked){
      var inputMessage = document.getElementById("text-message-body").value;
      console.log('Message entered: ' + inputMessage);
    }
    else{
      var inputMessage = null;
    }

    makeRequestToServer(inputAddr, inputPort, inputRequestType, inputKey, inputMessage);

}

function makeRequestToServer(inputAddr, inputPort, inputRequestType, inputKey, inputMessage){
    console.log('entered makeCallToServer');
    var xhr = new XMLHttpRequest(); // creating request object
    if(inputKey != null){
      var url = inputAddr + ":" + inputPort + "/movies/" + inputKey;
    }
    else{
      var url = inputAddr + ":" + inputPort + "/movies/";
    }
    console.log(url)
    xhr.open(inputRequestType, url, true);

    xhr.onload = function(e) {
        //console.log(xhr.responseText);
        updateTextWithResponse(xhr.responseText, inputKey, inputRequestType, inputMessage);
    }
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(inputMessage);


}

function updateTextWithResponse(response_text, inputKey, inputRequestType, inputMessage){
    var response_json = JSON.parse(response_text);
    console.log("Entered updateText")
    if(inputMessage != null){
      var message_json = JSON.parse(inputMessage);
    }
    //updating labels
    var label1 = document.getElementById("response-label");
    var label2 = document.getElementById("answer-label");
    label1.innerHTML = response_text;
    if(response_json['result']== 'success'){
      if(inputKey != null){
        label1.innerHTML = response_text;
        if(inputRequestType == 'GET'){
          label2.innerHTML = response_json['title'] + " belongs to the following genre(s): " + response_json['genres'];
        }
        else if(inputRequestType == 'PUT'){
          label2.innerHTML = "Movie " + message_json['title'] + " was successfully updated";
        }
        else if(inputRequestType == 'DELETE'){
          label2.innerHTML = "Movie with id: " + inputKey + ", was successfully deleted";
        }
      }
      else{
        if(inputRequestType == 'GET'){
          var str = "List of movies: ";
          new_dict = response_json['movies'];
          for(var index = 0; index < new_dict.length; index++){
            //console.log(new_dict[index]);
            label2.innerHTML += "ID: " + new_dict[index]['id'] + ", Title: " + new_dict[index]['title'] + "\n";
          }

        }
        else if(inputRequestType == 'POST'){
            label2.innerHTML = "Movie "+ message_json['title'] + " was added successfully with ID: "+response_json['id'];

        }
        else if(inputRequestType == 'DELETE'){
          label2.innerHTML = "Database cleared"
        }
      }
    }
    else{
      label2.innerHTML = "Something went wrong :(";
    }
}
